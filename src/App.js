import React, { Component } from "react";
import "./App.css";
import Player from "./components/choosePlayer";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      player: "X",
      board: Array(9).fill(null),
      boxNum: Array.apply(null, { length: 9 }).map(Number.call, Number),
      winner: null
    };
  }

  checkWinner() {
    let winLines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6]
    ];

    for (let i = 0; i < winLines.length; i++) {
      let [a, b, c] = winLines[i];
      if (
        this.state.board[a] !== null &&
        this.state.board[b] !== null &&
        this.state.board[c] !== null
      ) {
        if (
          this.state.board[a] === this.state.board[b] &&
          this.state.board[a] === this.state.board[c]
        ) {
          this.setState({
            winner: this.state.player
          });
          alert("Win");
        }
      }
    }
  }

  handleClick(index) {
    if (this.state.board[index] === null && this.state.winner === null) {
      let newBoard = this.state.board;
      newBoard[index] = this.state.player;
      let newPlayer = this.state.player === "X" ? "O" : "X";

      this.setState({
        board: newBoard,
        player: newPlayer
      });
      console.log(index);
      this.checkWinner();
    }
  }

  render() {
    const newBox = this.state.boxNum.map((_, i) => (
      <div
        onClick={() => {
          this.handleClick(i);
        }}
        className="box"
        key={this.state.boxNum[i]}
      >
        {this.state.board[i]}
      </div>
    ));

    return (
      <div className="container">
        <h1>Tic Tac Toy</h1>
        <Player />
        <div className="board">{newBox}</div>
      </div>
    );
  }
}

export default App;
