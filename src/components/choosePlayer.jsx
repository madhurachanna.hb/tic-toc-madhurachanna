import React, { Component } from "react";

class Player extends Component {
  state = {};
  render() {
    return (
      <form onSubmit={e => this.handleForm(e)}>
        <label htmlFor="">
          Player X
          <input type="radio" name="player" value="X" />
        </label>
        <label htmlFor="">
          Player O
          <input type="radio" name="player" value="O" />
        </label>
        <input type="submit" value="Start" />
      </form>
    );
  }
}

export default Player;
